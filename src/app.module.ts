import { Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { RealEstateModule } from '@real-estate/real-estate.module';
import { StockMarketModule } from './module/stock-market/stock-market.module';

@Module({
    imports: [
        RealEstateModule,
        StockMarketModule
    ]
})
export class AppModule { }
