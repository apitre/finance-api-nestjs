import { MortgageParams } from '@real-estate/entity/mortgage.params';
import { MortgageCalculator } from '@real-estate/service/mortgage.calculator';
import { MortgageResponse } from '@real-estate/entity/mortgage.response';
import { Injectable } from '@nestjs/common';

@Injectable()
export class MortgageHandler 
{
    static readonly DATE_FORMAT: string = "yyyy-MM-dd";
    static readonly COMPOUND: number = 2;

    constructor(private mortgageCalculator: MortgageCalculator) { }

    public handlerCalculation(mortgage: MortgageParams): MortgageResponse
    {
        return {
            detail: "Mortgage calculated sucessfully",
            errors: [],
            data: this.mortgageCalculator.calculatePayment(mortgage)
        }
    }
}