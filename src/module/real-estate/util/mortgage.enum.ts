export enum Period {
    MONTHLY = "monthly",
    SEMI_MONTHLY =  "semi-monthly",
    WEEKLY = "weekly",
    BI_WEEKLY = "bi-weekly"
}