import { Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { MortgageController } from '@real-estate/controller/mortgage.controller';
import { MortgageHandler } from '@real-estate/handler/mortgage.handler';
import { MortgageCalculator } from '@real-estate/service/mortgage.calculator';

@Module({
    imports: [
        JwtModule.register({secret: 'hard!to-guess_secret'})
    ],
    controllers: [
        MortgageController
    ],
    providers: [
        MortgageHandler,
        MortgageCalculator
    ]
})
export class RealEstateModule { }
