import { Period } from "@real-estate/util/mortgage.enum";

export interface MortageResult
{
    period: Period;
    payment: number;
    numberOfPayments: number;
    schl: number;
}