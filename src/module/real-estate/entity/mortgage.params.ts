import { IsEnum, IsISO8601, IsNumber, IsOptional, Max, Min } from "class-validator";
import { Period } from "@real-estate/util/mortgage.enum";

export class MortgageParams
{
    @IsISO8601()
    startDate: string;

    @Min(0)
    @Max(100)
    @IsNumber()
    interest: number;

    @IsEnum(Period)
    period: Period;

    @Min(0)
    @IsNumber()
    propertyPrice: number;

    @Min(0)
    @Max(30)
    @IsNumber()
    term: number;
    
    @Min(0)
    @IsNumber()
    cashDown: number;

    @IsNumber()
    @IsOptional()
    taxes?: number;

    @IsNumber()
    @IsOptional()
    rentPerMonth?: number;
}