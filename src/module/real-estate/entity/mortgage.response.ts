import { FinanceResponse } from "@common/entity/fincance.response";
import { MortageResult } from "./mortgage.result";

export interface MortgageResponse extends FinanceResponse
{
    data: MortageResult;
}