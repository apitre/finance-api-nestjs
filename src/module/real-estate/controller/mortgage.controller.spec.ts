import * as request from 'supertest';
import { Test } from '@nestjs/testing';
import { INestApplication, ModuleMetadata } from '@nestjs/common';
import { RealEstateModule } from '@real-estate/real-estate.module';
import { HttpExceptionFilter } from '@common/exception/http.exception.filter';

describe('MortgageController', () => {

    let app: INestApplication;

    beforeAll(async () => {
        const metadata: ModuleMetadata = {
            imports: [RealEstateModule]
        };
        const moduleRef = await Test.createTestingModule(metadata).compile();
        app = moduleRef.createNestApplication();
        app.useGlobalFilters(new HttpExceptionFilter());
        await app.init();
    });

    it(`/GET calculate mortgage forbidden`, () => {
        return request(app.getHttpServer())
            .get('/mortgage/calculator')
            .expect(403)
            .expect({
                detail: 'Forbidden',
                errors: ['Missing header authorization'],
                data: null
            });
    });

    it(`/GET calculate mortgage success`, () => {
        return request(app.getHttpServer())
            .get('/mortgage/calculator')
            .set('Authorization', 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6ImRlbW9fdXNlciIsImlhdCI6MTYxMzI3ODI4NH0.OxSf0o_TVmHeQVZUWPhn9s7Dzu9uTYYhzZrWdwC4ccE')
            .query({
                startDate: '2020-11-18 ',
                interest: '2.10',
                period: 'monthly',
                propertyPrice: '169000',
                term: '25',
                cashDown: '15000'
            })
            .expect(200)
            .expect({
                detail: 'Mortgage calculated sucessfully',
                errors: [],
                data: { 
                    period: 'monthly', 
                    numberOfPayments: 300,
                    payment: 685.95,
                    schl: 6160 
                }
            });
    });

    afterAll(async () => {
        await app.close();
    });

});