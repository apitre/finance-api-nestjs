import { Controller, Get, Query, UseGuards } from '@nestjs/common';
import { MortgageResponse } from '@real-estate/entity/mortgage.response';
import { MortgageHandler } from '@real-estate/handler/mortgage.handler';
import { MortgageParams } from '@real-estate/entity/mortgage.params';
import { AuthGuard } from '@common/guard/auth.guard';

@UseGuards(AuthGuard)
@Controller("/mortgage")
export class MortgageController 
{
    constructor(private mortgageHandler: MortgageHandler) {}

    @Get("calculator")
    async morgage(@Query() morgage: MortgageParams): Promise<MortgageResponse> {
        return this.mortgageHandler.handlerCalculation(morgage);
    }
}
