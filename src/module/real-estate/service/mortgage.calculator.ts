import * as moment from 'moment';
import { Moment } from "moment";
import { MortgageParams } from '@real-estate/entity/mortgage.params';
import { Period } from '@real-estate/util/mortgage.enum';
import { MortageResult } from '@real-estate/entity/mortgage.result';
import { Injectable } from '@nestjs/common';
import { AmountFormat } from '@common/util/amont.format';

@Injectable()
export class MortgageCalculator 
{
    static readonly DATE_FORMAT: string = "yyyy-MM-dd";
    static readonly COMPOUND_PER_YEAR: number = 2;
    static readonly SCHL: Array<[number, number]> = [
        [0.05, 0.0450], 
        [0.10, 0.0400], 
        [0.15, 0.0310],
        [0.20, 0.0280]
    ];

    public getSCHL(mortgage: MortgageParams): number
    {
        const cashdownPourcent: number = mortgage.cashDown / mortgage.propertyPrice;

        for (let i = 0; i < MortgageCalculator.SCHL.length; i++) {
            if (cashdownPourcent < MortgageCalculator.SCHL[i][0]) {
                return MortgageCalculator.SCHL[i][1];
            }
        }

        return 0;
    }

    private numberOfPayments(startStringDate: string, numberOfYear: number, period: Period): number
    {
        let startDate: Moment = moment(startStringDate, MortgageCalculator.DATE_FORMAT);
        let endDate: Moment = moment(new Date(startStringDate)).add(numberOfYear, "years");

        if (period === Period.BI_WEEKLY) {
            return endDate.diff(startDate, "weeks") / 2;
        }

        if (period === Period.SEMI_MONTHLY) {
            return endDate.diff(startDate, "months") * 2; 
        }

        if (period === Period.WEEKLY) {
            return endDate.diff(startDate, "weeks");
        }

        return endDate.diff(startDate, "months");
    }

    public calculatePayment(mortgage: MortgageParams): MortageResult 
    {
        let loanAmount: number = (mortgage.propertyPrice - mortgage.cashDown);

        let schl: number = loanAmount * this.getSCHL(mortgage);

        let numberOfPayments = this.numberOfPayments(mortgage.startDate, mortgage.term, mortgage.period);

        let interestPerCompound: number = 1 + (mortgage.interest / 100) / MortgageCalculator.COMPOUND_PER_YEAR;

        let compoundDistributedForOneYear: number = MortgageCalculator.COMPOUND_PER_YEAR / (numberOfPayments / mortgage.term);

        let yearCompoundInterest = Math.pow(interestPerCompound, compoundDistributedForOneYear); 
        
        let termCompoundInterest = Math.pow(yearCompoundInterest, -numberOfPayments);

        let payment: number = +((loanAmount + schl) * (yearCompoundInterest - 1) / (1 - termCompoundInterest)).toFixed(2);

        var numberOfPaymentsPerYear = this.numberOfPayments(mortgage.startDate, 1, mortgage.period);

        if (mortgage.rentPerMonth !== undefined && mortgage.rentPerMonth > 0) {
            payment -= ((mortgage.rentPerMonth * 12) / numberOfPaymentsPerYear);
        }

        if (mortgage.taxes !== undefined && mortgage.taxes > 0) {
            payment += (mortgage.taxes / numberOfPaymentsPerYear);
        }

        return {
            period: mortgage.period,
            numberOfPayments: numberOfPayments,
            payment: AmountFormat.toFixed(payment),
            schl: AmountFormat.toFixed(schl)
        };
    }

}
