import { MortgageParams } from '@real-estate/entity/mortgage.params';
import { MortageResult } from '@real-estate/entity/mortgage.result';
import { MortgageCalculator } from '@real-estate/service/mortgage.calculator';
import { Period } from '@real-estate/util/mortgage.enum';

describe('Mortgage ', () => {

    var calculator: MortgageCalculator = new MortgageCalculator();
    var mortgage: MortgageParams;

    beforeEach(() => {
        mortgage = {
            startDate: "2020-11-18",
            interest: 3,
            period: Period.MONTHLY,
            propertyPrice: 200000,
            term: 25,
            cashDown: 10000
        };
    });

    describe('Calculate mortgage by period ', () => {

        it('Should associate the right SCHL rate', () => {
            mortgage.cashDown = 9999;
            expect(calculator.getSCHL(mortgage)).toEqual(0.045);
            mortgage.cashDown = 10000;
            expect(calculator.getSCHL(mortgage)).toEqual(0.040);
            mortgage.cashDown = 19999;
            expect(calculator.getSCHL(mortgage)).toEqual(0.040);
            mortgage.cashDown = 20000;
            expect(calculator.getSCHL(mortgage)).toEqual(0.031);
            mortgage.cashDown = 29999;
            expect(calculator.getSCHL(mortgage)).toEqual(0.031);
            mortgage.cashDown = 30000;
            expect(calculator.getSCHL(mortgage)).toEqual(0.028);
            mortgage.cashDown = 39999;
            expect(calculator.getSCHL(mortgage)).toEqual(0.028);
            mortgage.cashDown = 40000;
            expect(calculator.getSCHL(mortgage)).toEqual(0);
        });

        it('Should calculate for default 25 years term', () => {
            mortgage.propertyPrice = 320000;
            mortgage.cashDown = 50000;
            mortgage.interest = 3.09;
            expect(calculator.calculatePayment(mortgage)).toEqual({
                period: Period.MONTHLY,
                numberOfPayments: 300,
                payment: 1326.39,
                schl: 7560
            });
        });

        it('Should calculate for 20 years term', () => {
            mortgage.term = 20;
            expect(calculator.calculatePayment(mortgage)).toEqual({
                period: Period.MONTHLY,
                numberOfPayments: 240,
                payment: 1094.05,
                schl: 7600
            });
        });

        it('Should calculate for 5.5 interest', () => {
            mortgage.interest = 5.5;
            expect(calculator.calculatePayment(mortgage)).toEqual({
                period: Period.MONTHLY,
                numberOfPayments: 300,
                payment: 1206.13,
                schl: 7600
            });
        });

        it('Should calculate for monthly payment', () => {
            mortgage.period = Period.MONTHLY;
            expect(calculator.calculatePayment(mortgage)).toEqual({
                period: Period.MONTHLY,
                numberOfPayments: 300,
                payment: 935.13,
                schl: 7600
            });
        });

        it('Should calculate for semi-monthly payment', () => {
            mortgage.propertyPrice = 200000;
            mortgage.cashDown = 10000;
            mortgage.period = Period.SEMI_MONTHLY;
            expect(calculator.calculatePayment(mortgage)).toEqual({
                period: Period.SEMI_MONTHLY,
                numberOfPayments: 600,
                payment: 467.28,
                schl: 7600
            });
        });
        
        it('Should calculate for weekly payment', () => {
            mortgage.period = Period.WEEKLY;
            expect(calculator.calculatePayment(mortgage)).toEqual({
                period: Period.WEEKLY,
                numberOfPayments: 1306,
                payment: 214.60,
                schl: 7600
            });
        });

        it('Should calculate for bi-weekly payment', () => {
            mortgage.period = Period.BI_WEEKLY;
            expect(calculator.calculatePayment(mortgage)).toEqual({
                period: Period.BI_WEEKLY,
                numberOfPayments: 653,
                payment: 429.33,
                schl: 7600
            });
        });

    });

    describe('Calculate mortgage with rents', () => {

        it('Should calculate with rents for monthly payment', () => {
            mortgage.period = Period.MONTHLY;
            mortgage.rentPerMonth = 400;
            expect(calculator.calculatePayment(mortgage)).toEqual({
                period: Period.MONTHLY,
                numberOfPayments: 300,
                payment: 535.13,
                schl: 7600
            });
        });

        it('Should calculate with rents for semi-monthly payment', () => {
            mortgage.period = Period.SEMI_MONTHLY;
            mortgage.rentPerMonth = 400;
            expect(calculator.calculatePayment(mortgage)).toEqual({
                period: Period.SEMI_MONTHLY,
                numberOfPayments: 600,
                payment: 267.28,
                schl: 7600
            });
        });

        it('Should calculate with rents for weekly payment', () => {
            mortgage.period = Period.WEEKLY;
            mortgage.rentPerMonth = 400;
            expect(calculator.calculatePayment(mortgage)).toEqual({
                period: Period.WEEKLY,
                numberOfPayments: 1306,
                payment: 125.71,
                schl: 7600
            });
        });

        it('Should calculate with rents for bi-monthly payment', () => {
            mortgage.period = Period.BI_WEEKLY;
            mortgage.rentPerMonth = 400;
            expect(calculator.calculatePayment(mortgage)).toEqual({
                period: Period.BI_WEEKLY,
                numberOfPayments: 653,
                payment: 251.55,
                schl: 7600
            });
        });

    });

    describe('Calculate mortgage with taxes', () => {

        it('Should calculate with taxes for monthly payment', () => {
            mortgage.period = Period.MONTHLY;
            mortgage.taxes = 1000;
            expect(calculator.calculatePayment(mortgage)).toEqual({
                period: Period.MONTHLY,
                numberOfPayments: 300,
                payment: 1018.46,
                schl: 7600
            });
        });

        it('Should calculate with taxes for semi-monthly payment', () => {
            mortgage.period = Period.SEMI_MONTHLY;
            mortgage.taxes = 1000;
            expect(calculator.calculatePayment(mortgage)).toEqual({
                period: Period.SEMI_MONTHLY,
                numberOfPayments: 600,
                payment: 508.95,
                schl: 7600
            });
        });

        it('Should calculate with taxes for weekly payment', () => {
            mortgage.period = Period.WEEKLY;
            mortgage.taxes = 1000;
            expect(calculator.calculatePayment(mortgage)).toEqual({
                period: Period.WEEKLY,
                numberOfPayments: 1306,
                payment: 233.12,
                schl: 7600
            });
        });

        it('Should calculate with taxes for bi-monthly payment', () => {
            mortgage.period = Period.BI_WEEKLY;
            mortgage.taxes = 1000;
            expect(calculator.calculatePayment(mortgage)).toEqual({
                period: Period.BI_WEEKLY,
                numberOfPayments: 653,
                payment: 466.37,
                schl: 7600
            });
        });

    });
});
