import { AmountFormat } from '@common/util/amont.format';
import { Injectable } from '@nestjs/common';
import { DividendInvestmentResult } from '@stock-market/entity/dividend.investment.result';
import { Investment } from '@stock-market/entity/investment';
import { InvestmentDividend } from '@stock-market/entity/investment.dividend';
import { InvestmentResult } from '@stock-market/entity/investment.result';

@Injectable()
export class FormulaService 
{
    private time: (x: number) => (fn: () => void) => void = x => fn => {
        if (x > 0 ) {
            fn();
            this.time (x - 1) (fn)
        }
    }
    
    calculateInvestment(investment: Investment): InvestmentResult{
        let value: number = investment.initialAmount;
        let growthValues: Array<number> = [];

        this.time (investment.term) (() => {
            this.time (investment.compoundFrequency) (() => value *= 1 + (investment.growthRate / investment.compoundFrequency / 100));
            value += investment.annualContribution;
            growthValues.push(AmountFormat.toFixed(value));
        }); 

        return {
            value: AmountFormat.toFixed(value),
            growthValues: growthValues
        }
    }

    calculateShareInvestment(investment: InvestmentDividend): DividendInvestmentResult {

        let shareQuantity: number = investment.initialAmount / investment.sharePrice;
        let sharePrice: number = investment.sharePrice;
        let dividendPaid: number = 0;  
        let dividendReinvested: number = 0; 
        let year: number = 0;      

        this.time (investment.term) (() => {

            year++;

            let dividend: number = sharePrice * shareQuantity * (investment.dividend / 100);

            let dividendToReinvest: number = dividend * (investment.reinvestRate / 100);

            let sharePriceGrowth: number = sharePrice * (investment.growthRate / 100 / investment.compoundFrequency);

            this.time (investment.compoundFrequency) (() => {
                //shareQuantity += dividendToReinvest / investment.compoundFrequency / sharePrice;  
                
                sharePrice += sharePriceGrowth;
                shareQuantity += (sharePrice * shareQuantity * (investment.dividend / 100 / investment.compoundFrequency)) / sharePrice;  
  
            });

            //console.log(shareQuantity);

            //sharePrice = AmountFormat.toFixed(sharePrice);

            dividendReinvested += dividendToReinvest;
            dividendPaid += dividend;

            //shareQuantity += investment.annualContribution / sharePrice;

            console.log(year, dividend, shareQuantity, sharePrice, sharePrice*shareQuantity);

        }); 

        return {
            moneyInvested: AmountFormat.toFixed(investment.initialAmount + (investment.annualContribution * investment.term)),
            dividendPaid: AmountFormat.toFixed(dividendPaid),
            dividendReinvested: AmountFormat.toFixed(dividendReinvested),
            sharePrice: AmountFormat.toFixed(sharePrice),
            shareQuantity: AmountFormat.toFixed(shareQuantity),
            marketValue: AmountFormat.toFixed(shareQuantity * sharePrice),
            totalValue: AmountFormat.toFixed(dividendPaid - dividendReinvested + (shareQuantity * sharePrice))
        }
    }

}
