import { InvestmentDividend } from '@stock-market/entity/investment.dividend';
import { CompoundFrequency } from '../util/compound.frequency';
import { FormulaService } from './formula.service';

describe('Investment Calculation QUARTERLY ', () => {

    const service: FormulaService = new FormulaService();
    let investment: InvestmentDividend;

    beforeEach(() => {
        investment = new InvestmentDividend();
        investment.compoundFrequency = CompoundFrequency.QUARTERLY;
    });

    it('should calculate quarterly-case-1', () => {
        investment.initialAmount = 0;
        investment.annualContribution = 1200;
        investment.growthRate = 0;
        investment.term = 25;
        investment.dividend = 3;
        investment.sharePrice = 5;
        investment.reinvestRate = 0;   
        expect(service.calculateShareInvestment(investment)).toEqual({
            moneyInvested: 30000,
            dividendPaid: 10800,
            dividendReinvested: 0,
            sharePrice: 5,
            shareQuantity: 6000,
            marketValue: 30000,
            totalValue: 40800
        })
    });

    it('should calculate quarterly-case-2', () => {
        investment.initialAmount = 0;
        investment.annualContribution = 1200;
        investment.growthRate = 2;
        investment.term = 25;
        investment.dividend = 3;
        investment.sharePrice = 5;
        investment.reinvestRate = 0;    
        expect(service.calculateShareInvestment(investment)).toEqual({
            moneyInvested: 30000,
            dividendPaid: 12654.54,
            dividendReinvested: 0,
            sharePrice: 8.2,
            shareQuantity: 4685.63,
            marketValue: 38436.36,
            totalValue: 51090.9
        })
    });

    it('should calculate quarterly-case-3', () => {
        investment.initialAmount = 0;
        investment.annualContribution = 1200;
        investment.growthRate = 2;
        investment.term = 25;
        investment.dividend = 0;
        investment.sharePrice = 5;
        investment.reinvestRate = 0;    
        expect(service.calculateShareInvestment(investment)).toEqual({
            moneyInvested: 30000,
            dividendPaid: 0,
            dividendReinvested: 0,
            sharePrice: 8.2,
            shareQuantity: 4685.63,
            marketValue: 38436.36,
            totalValue: 38436.36
        })
    });

    it('should calculate quarterly-case-4', () => {
        investment.initialAmount = 5000;
        investment.annualContribution = 0;
        investment.growthRate = 2;
        investment.term = 25;
        investment.dividend = 3;
        investment.sharePrice = 5;
        investment.reinvestRate = 100;    
        expect(service.calculateShareInvestment(investment)).toEqual({
            moneyInvested: 30000,
            dividendPaid: 16363.51,
            dividendReinvested: 16363.51,
            sharePrice: 8.2,
            shareQuantity: 6981.87,
            marketValue: 57272.52,
            totalValue: 57272.52
        })
    });

    it('should calculate quarterly-case-5', () => {
        investment.initialAmount = 0;
        investment.annualContribution = 1200;
        investment.growthRate = 2;
        investment.term = 25;
        investment.dividend = 3;
        investment.sharePrice = 5;
        investment.reinvestRate = 50;    
        expect(service.calculateShareInvestment(investment)).toEqual({
            moneyInvested: 30000,
            dividendPaid: 14348.42,
            dividendReinvested: 7174.21,
            sharePrice: 8.2,
            shareQuantity: 5697.87,
            marketValue: 46739.83,
            totalValue: 53914.04
        })
    });

    it('should calculate quarterly-case-6', () => {
        investment.initialAmount = 0;
        investment.annualContribution = 1200;
        investment.growthRate = 2;
        investment.term = 0;
        investment.dividend = 3;
        investment.sharePrice = 5;
        investment.reinvestRate = 100;    
        expect(service.calculateShareInvestment(investment)).toEqual({
            moneyInvested: 0,
            dividendPaid: 0,
            dividendReinvested: 0,
            sharePrice: 5,
            shareQuantity: 0,
            marketValue: 0,
            totalValue: 0
        })
    });

    it('should calculate quarterly-case-7', () => {
        investment.initialAmount = 0;
        investment.annualContribution = 1200;
        investment.growthRate = 2;
        investment.term = 1;
        investment.dividend = 3;
        investment.sharePrice = 5;
        investment.reinvestRate = 100;    
        expect(service.calculateShareInvestment(investment)).toEqual({
            moneyInvested: 1200,
            dividendPaid: 0,
            dividendReinvested: 0,
            sharePrice: 5.1,
            shareQuantity: 235.29,
            marketValue: 1200,
            totalValue: 1200
        })
    });

    it('should calculate quarterly-case-8', () => {
        investment.initialAmount = 0;
        investment.annualContribution = 1200;
        investment.growthRate = 2;
        investment.term = 2;
        investment.dividend = 3;
        investment.sharePrice = 5;
        investment.reinvestRate = 100;    
        expect(service.calculateShareInvestment(investment)).toEqual({
            moneyInvested: 2400,
            dividendPaid: 36,
            dividendReinvested: 36,
            sharePrice: 5.2,
            shareQuantity: 472.9,
            marketValue: 2460,
            totalValue: 2460
        })
    });

    it('should calculate quarterly-case-9', () => {
        investment.initialAmount = 3000;
        investment.annualContribution = 0;
        investment.growthRate = 2;
        investment.term = 1;
        investment.dividend = 3;
        investment.sharePrice = 5;
        investment.reinvestRate = 0;    
        expect(service.calculateShareInvestment(investment)).toEqual({
            moneyInvested: 3000,
            dividendPaid: 90,
            dividendReinvested: 0,
            sharePrice: 5.1,
            shareQuantity: 600,
            marketValue: 3060,
            totalValue: 3150
        })
    });

    it('should calculate quarterly-case-10', () => {
        investment.initialAmount = 3000;
        investment.annualContribution = 0;
        investment.growthRate = 2;
        investment.term = 1;
        investment.dividend = 3;
        investment.sharePrice = 5;
        investment.reinvestRate = 100;    
        expect(service.calculateShareInvestment(investment)).toEqual({
            moneyInvested: 3000,
            dividendPaid: 90,
            dividendReinvested: 90,
            sharePrice: 5.1,
            shareQuantity: 617.65,
            marketValue: 3150,
            totalValue: 3150
        })
    });
    
});
