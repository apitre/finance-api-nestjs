import { Controller, Get, Query, UseGuards } from '@nestjs/common';
import { MortgageResponse } from '@real-estate/entity/mortgage.response';
import { MortgageHandler } from '@real-estate/handler/mortgage.handler';
import { AuthGuard } from '@common/guard/auth.guard';

@UseGuards(AuthGuard)
@Controller("/stockmarket/formula")
export class FormulaController 
{
    constructor(private mortgageHandler: MortgageHandler) {}

    @Get("compound")
    async morgage(): Promise<MortgageResponse> {
        return 
    }
}
