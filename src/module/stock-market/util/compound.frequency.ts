export enum CompoundFrequency {
    YEARLY = 1,
    MONTHLY =  12,
    QUARTERLY = 4,
    SEMI_ANNUALY = 2
}