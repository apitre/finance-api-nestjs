export interface DividendInvestmentResult
{
    moneyInvested: number;
    dividendPaid: number;
    dividendReinvested: number;
    sharePrice: number;
    shareQuantity: number;
    marketValue: number;
    totalValue: number;
}