import { IsEnum, IsNumber, Max, Min } from "class-validator";
import { CompoundFrequency } from "../util/compound.frequency";

export class Investment
{
    @Min(0)
    @Max(100)
    @IsNumber()
    growthRate: number;

    @Min(0)
    @IsNumber()
    term: number;

    @IsEnum(CompoundFrequency)
    compoundFrequency: CompoundFrequency;

    @Min(0)
    @IsNumber()
    annualContribution: number;

    @Min(0)
    @IsNumber()
    initialAmount: number;
}