import { IsEnum, IsNumber, Max, Min } from "class-validator";
import { CompoundFrequency } from "@stock-market/util/compound.frequency";
import { Investment } from "@stock-market/entity/investment";

export class InvestmentDividend extends Investment
{
    @Min(0)
    @IsNumber()
    sharePrice: number;

    @Min(0)
    @Max(100)
    @IsNumber()
    dividend: number;

    @Min(0)
    @Max(100)
    @IsNumber()
    reinvestRate: number;
}