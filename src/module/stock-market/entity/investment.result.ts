import { Period } from "@real-estate/util/mortgage.enum";

export interface InvestmentResult
{
    value: number;
    growthValues: Array<number>;
}