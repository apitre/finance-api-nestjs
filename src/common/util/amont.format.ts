export class AmountFormat 
{
    static toFixed(number: number): number
    {
        return +number.toFixed(2);
    }
}
