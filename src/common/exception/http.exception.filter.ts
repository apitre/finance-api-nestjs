
import { FinanceResponse } from '@common/entity/fincance.response';
import { ExceptionFilter, Catch, ArgumentsHost, HttpException } from '@nestjs/common';
import { isArray } from 'class-validator';
import { Request, Response } from 'express';

@Catch(HttpException)
export class HttpExceptionFilter implements ExceptionFilter {
  catch(exception: HttpException, host: ArgumentsHost) {
    const response = host.switchToHttp().getResponse<Response>();
    const exMsg: any = exception.getResponse();
    response
        .status(exception.getStatus())
        .json({
            detail: exMsg.error,
            errors: isArray(exMsg.message) ?  exMsg.message : [exMsg.message],
            data: null
        } as FinanceResponse);
  }
}
