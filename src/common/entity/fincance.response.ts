export interface FinanceResponse
{
    detail: string;
    errors: string[];
    data: any;
}