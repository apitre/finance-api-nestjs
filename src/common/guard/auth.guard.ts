
import { AuthUser } from '@common/entity/auth.user';
import { Injectable, CanActivate, ExecutionContext, ForbiddenException } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { Observable } from 'rxjs';

@Injectable()
export class AuthGuard implements CanActivate 
{
    constructor(private jwtService: JwtService) {}

    canActivate(context: ExecutionContext): boolean | Promise<boolean> | Observable<boolean> 
    {
        const request: any = context.switchToHttp().getRequest();

        if (request.headers.authorization === undefined) {
            throw new ForbiddenException("Missing header authorization");
        }

        try {
            return this.jwtService.verify<AuthUser>(request.headers.authorization).username === 'demo_user';
        } catch (e) {
            return false;
        }
    }
}
